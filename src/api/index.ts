import {RealTimeAPI} from 'rocket.chat.realtime.api.rxjs'

export default {
  connectToRocketChat(rocketChatRealTimeApiUrl: any) {
    return new RealTimeAPI (rocketChatRealTimeApiUrl)
  }
}