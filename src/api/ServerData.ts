import axios, { AxiosResponse, AxiosRequestConfig, AxiosError } from 'axios';

// Set up API request config
function axiosContext(): AxiosRequestConfig {
  return {
    // withCredentials: true,
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
    //   "X-Auth-Token": "KOXJb7SI3VIfkJPLumdZIOkjY_3XOKqZsfC2ANA_vzW",
      "X-Auth-Token": "qhPMskSQtywq7S3cxnHYx2dPCo98Nnws_UfHn8YHhHd",
    //   "X-User-Id": "uAqGG4SQARbMsuart",
      "X-User-Id": "W5CDbvu7DP3aA4aHR",
    //   "Access-Control-Allow-Credentials": true
    },
  };
}

function getBaseUrl(): string {
    return "https://vishalbeehyv.rocket.chat";
}

function getCreateChannelUrl(): string {
  return getBaseUrl() + "/api/v1/channels.create";
}

function getCreateGroupUrl(): string {
  return getBaseUrl() + "/api/v1/groups.create";
}

function getCreateDirectChatUrl(): string {
  return getBaseUrl() + "/api/v1/im.create";
}

function getRoomsUrl(): string {
  return getBaseUrl() + "/api/v1/rooms.get";
}

function getUsersListUrl(): string {
  return getBaseUrl() + "/api/v1/users.list";
}

function getUpdatesUrl(): string {
  return getBaseUrl() + "/api/v1/subscriptions.get";
}

function getCurrentLoginDetailsUrl(): string {
  return getBaseUrl() + "/api/v1/me";
}

function getMarkAsReadUrl(): string {
  return getBaseUrl() + "/api/v1/subscriptions.read";
}

function getPostMsgUrl(): string {
  return getBaseUrl() + "/api/v1/chat.sendMessage";
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function httpGet(url: string): Promise<AxiosResponse<any>> {
  return new Promise((resolve, reject) => {
    axios.get(url, axiosContext()).then((obj) => {
      resolve(obj);
    }).catch((error: AxiosError) => {
      reject(error);
    });
  });
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function httpPost(url: string, payload: any): Promise<AxiosResponse<any>> {
    return new Promise((resolve, reject) => {
      axios.post(url, payload, axiosContext()).then((obj) => {
        resolve(obj);
      }).catch((error: AxiosError) => {
        reject(error);
      });
    });
  }

export default class ServerData {
  public static createChannel(payload: any): Promise<void> {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      httpPost(getCreateChannelUrl(), payload).then((response) => {
        console.log("create channel response", response);
        resolve(response);
      }).catch((error: any) => {
        reject(error);
      });
    });
  }

  public static createGroup(payload: any): Promise<void> {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      httpPost(getCreateGroupUrl(), payload).then((response) => {
        console.log("create group response", response);
        resolve(response);
      }).catch((error: any) => {
        reject(error);
      });
    });
  }

  public static createDirectChat(payload: any): Promise<void> {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      httpPost(getCreateDirectChatUrl(), payload).then((response) => {
        console.log("create direct chat response", response);
        resolve(response);
      }).catch((error: any) => {
        reject(error);
      });
    });
  }

  public static getRooms(): Promise<any> {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      httpGet(getRoomsUrl()).then((response) => {
        console.log("get rooms response", response);
        resolve(response);
      }).catch((error: any) => {
        reject(error);
      });
    });
  }

  public static getUsers(): Promise<any> {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      httpGet(getUsersListUrl()).then((response) => {
        console.log("get users response", response);
        resolve(response);
      }).catch((error: any) => {
        reject(error);
      });
    });
  }

  public static getUpdates(): Promise<any> {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      httpGet(getUpdatesUrl()).then((response) => {
        // console.log("get users response", response);
        resolve(response);
      }).catch((error: any) => {
        reject(error);
      });
    });
  }

  public static getCurrentUserDetails(): Promise<any> {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      httpGet(getCurrentLoginDetailsUrl()).then((response) => {
        console.log("get current user response", response);
        resolve(response);
      }).catch((error: any) => {
        reject(error);
      });
    });
  }

  public static markRoomAsRead(payload: any): Promise<void> {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      httpPost(getMarkAsReadUrl(), payload).then((response) => {
        resolve();
      }).catch((error: any) => {
        reject(error);
      });
    });
  }

  public static postMessageToRoom(payload: any): Promise<void> {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      httpPost(getPostMsgUrl(), payload).then((response) => {
        console.log("post message to room resp", response);
      }).catch((error: any) => {
        reject(error);
      });
    });
  }  
}